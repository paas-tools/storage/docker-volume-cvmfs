package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/docker/docker/api/types/volume"
	"github.com/docker/docker/client"
	"github.com/spf13/cobra"
	"golang.org/x/net/context"
)

// KubeOptions has the options to be passed to the kube pod.
type KubeOptions struct {
	Mountpoint string `json:"mountpoint"`
	Repository string `json:"repository"`
}

var initCmd = &cobra.Command{
	Use:          "init",
	Short:        "init the cvmfs setup",
	Long:         "currently a noop for the cvmfs driver",
	SilenceUsage: true,
	Run: func(cmd *cobra.Command, args []string) {
		log.Printf("init\n")
	},
}

var versionCmd = &cobra.Command{
	Use:          "version",
	Short:        "print the versiona and exit",
	SilenceUsage: true,
	Run: func(cmd *cobra.Command, args []string) {
		log.Printf("%v\n", version)
	},
}

var attachCmd = &cobra.Command{
	Use:   "attach json",
	Short: "attach a given cvmfs repository",
	Long: `
Attach a given cvmfs repository for later usage (mount).

Expects a json structure as argument, which should look like:
{"repository": "atlas.cern.ch"}
`,
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return flexError(RepoNotFoundError{})
		}
		var options KubeOptions
		err := json.Unmarshal([]byte(args[0]), &options)
		if err != nil {
			return flexError(err)
		}

		docker, err := dockerClient()
		if err != nil {
			return flexError(err)
		}
		volume, err := docker.VolumeCreate(context.Background(),
			volume.VolumesCreateBody{Driver: "cvmfs", Name: options.Repository})
		if err != nil {
			return flexError(err)
		}
		flexSuccess(volume.Mountpoint)
		return nil
	},
}

var detachCmd = &cobra.Command{
	Use:   "detach repository",
	Short: "detach a given cvmfs repository",
	Long: `
Detach the given cvmfs repository.

Expects the repository to detach as single argument.
`,
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return flexError(RepoNotFoundError{})
		}
		// We have no way to track the number of mounts for each volume, so for now
		// we can simply ignore the detach. The volumes stay in docker forever once
		// requested (not so bad as the amount of data client side is only the cache).
		flexSuccess("")
		return nil
	},
}

var mountCmd = &cobra.Command{
	Use:   "mount target source [json]",
	Short: "bind mount the given source dir in target",
	Long: `
Bind mounts the given source directory into target.
`,
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			err := RepoNotFoundError{}
			return flexError(err)
		}
		err := bindMount(args[1], args[0])
		if err != nil {
			return flexError(fmt.Errorf("bind mount failed :: %v", err))
		}
		flexSuccess("")
		return nil
	},
}

var umountCmd = &cobra.Command{
	Use:   "unmount directory",
	Short: "unmount the given directory",
	Long: `
Unmounts the given directory.
`,
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return flexError(RepoNotFoundError{})
		}

		err := umount(args[0])
		if err != nil {
			return flexError(err)
		}
		flexSuccess("")
		return nil
	},
}

func dockerClient() (*client.Client, error) {
	os.Setenv("DOCKER_API_VERSION", dockerVersion)
	return client.NewEnvClient()
}

func bindMount(src string, dest string) error {

	// check if mount directory exists, create if not
	_, err := os.Lstat(dest)
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("failed to check directory :: %v :: %v", dest, err)
	}
	if os.IsNotExist(err) {
		err = os.MkdirAll(dest, os.ModePerm)
		if err != nil {
			return fmt.Errorf("failed to create directory :: %v :: %v", dest, err)
		}
	}

	// bind mount src into dest
	cmd := exec.Command("mount", "--bind", src, dest)
	result, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to mount :: %v :: %v", err, string(result))
	}

	return nil
}

func umount(dir string) error {

	// umount the directory
	cmd := exec.Command("umount", dir)
	result, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to unmount :: %v :: %v", err, string(result))
	}

	return nil
}

func flexError(err error) error {
	fmt.Printf("{\"status\": \"Failure\", \"message\": \"%v\"}", err)
	return err
}

func flexSuccess(volumePath string) {
	if volumePath != "" {
		fmt.Printf("{\"status\": \"Success\", \"device\": \"%v\"}", volumePath)
	} else {
		fmt.Printf("{\"status\": \"Success\"}")
	}
}
